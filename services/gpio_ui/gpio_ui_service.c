/*******************************************************************************
 * Copyright 2019-2022 Microchip FPGA Embedded Systems Solutions.
 *
 * SPDX-License-Identifier: MIT
 *
 * MPFS HSS Embedded Software
 *
 */

/*!
 * \file tinycli Driver State Machine
 * \brief Virtualised tinycli Service
 */

#include "config.h"
#include "hss_types.h"
#include "hss_state_machine.h"
#include "hss_debug.h"
#include "hss_clock.h"

#include <string.h> //memset
#include <assert.h>

#include "gpio_ui_service.h"
#include "uart_helper.h"
#include "mpfs_reg_map.h"
#include "hss_boot_service.h"
#include "usbdmsc_service.h"

#include "drivers/mss/mss_mmuart/mss_uart.h"

static void gpio_ui_init_handler(struct StateMachine * const pMyMachine);
static void gpio_ui_inputs_handler(struct StateMachine * const pMyMachine);
static void gpio_ui_idle_handler(struct StateMachine * const pMyMachine);
static void gpio_ui_usbdmsc_handler(struct StateMachine * const pMyMachine);

/*!
 * \brief TINYCLI Driver States
 *
 */
enum UartStatesEnum {
    GPIO_UI_INITIALIZATION,
    GPIO_UI_READ_INPUTS,
    GPIO_UI_USBDMSC,
    GPIO_UI_IDLE,
    GPIO_UI_NUM_STATES = GPIO_UI_IDLE+1
};

/*!
 * \brief TINYCLI Driver State Descriptors
 *
 */
static const struct StateDesc gpio_ui_state_descs[] = {
    { (const stateType_t)GPIO_UI_INITIALIZATION, (const char *)"init",           NULL,                      NULL,                     &gpio_ui_init_handler },
    { (const stateType_t)GPIO_UI_READ_INPUTS,    (const char *)"readline",       NULL,                      NULL,                     &gpio_ui_inputs_handler },
    { (const stateType_t)GPIO_UI_USBDMSC,        (const char *)"usbdmsc",        NULL,                      NULL,                     &gpio_ui_usbdmsc_handler },
    { (const stateType_t)GPIO_UI_IDLE, (const char *)"uart_surrender", NULL,                      NULL,                     &gpio_ui_idle_handler }
};

/*!
 * \brief TINYCLI Driver State Machine
 *
 */
struct StateMachine gpio_ui_service = {
    .state             = (stateType_t)GPIO_UI_INITIALIZATION,
    .prevState         = (stateType_t)SM_INVALID_STATE,
    .numStates         = (const uint32_t)GPIO_UI_NUM_STATES,
    .pMachineName      = (const char *)"gpio_ui_service",
    .startTime         = 0u,
    .lastExecutionTime = 0u,
    .executionCount    = 0u,
    .pStateDescs       = gpio_ui_state_descs,
    .debugFlag         = false,
    .priority          = 0u,
    .pInstanceData     = NULL
};

// --------------------------------------------------------------------------------------------------
// Handlers for each state in the state machine
//

static void gpio_ui_init_handler(struct StateMachine * const pMyMachine)
{
    pMyMachine->state = GPIO_UI_READ_INPUTS;
}

/////////////////

static char myBuffer[HSS_UART_HELPER_MAX_GETLINE];
const size_t gpio_ui_bufferLen = ARRAY_SIZE(myBuffer)-1;
ssize_t gpio_ui_readStringLen = 0;

/////////////////

const char* gpio_ui_lineHeader = ">> ";

static void gpio_ui_inputs_handler(struct StateMachine * const pMyMachine)
{
    gpio_ui_service.state = GPIO_UI_USBDMSC;
}

/////////////////

static void gpio_ui_idle_handler(struct StateMachine * const pMyMachine)
{
    gpio_ui_service.state = GPIO_UI_IDLE;
}

/////////////////

static void gpio_ui_usbdmsc_handler(struct StateMachine * const pMyMachine)
{
#if IS_ENABLED(CONFIG_SERVICE_USBDMSC)

    bool done = false;
    uint8_t cBuf[1];

    done = !USBDMSC_IsActive();

    if (!done && (uart_getchar(cBuf, 0, false))) {
        done = (cBuf[0] == '\003') || (cBuf[0] == '\033');
    }

    if (done) {
        USBDMSC_Deactivate();
        pMyMachine->state = GPIO_UI_READ_INPUTS;
    }
#else
    pMyMachine->state = GPIO_UI_READ_INPUTS;
#endif
}

/////////////////
void HSS_GPIO_UI_WaitForUSBMSCDDone(void)
{
    gpio_ui_service.state = GPIO_UI_USBDMSC;
}

